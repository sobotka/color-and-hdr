.. Copyright 2020 Collabora, Ltd.


TODO
====

To allow optimal performance:

- always name the standard color space if one applies
- use the "simplest" ICC profile possible, that is, prefer a parametric
  description over a look-up table; the higher level the description,
  the more ways there are to implement it
- list the supported standard color spaces, so clients can be smarter? optimality?
- if you are a system designer, you can choose the color spaces used such that
  you can always off-load conversion to hardware

Chrome OS cannot afford to do 3D-LUT color conversions. They need to be able to
off-load all color space transformations to the display hardware. Hardware
gamma LUT is a given, CTM possibly, 3D-LUT not. They also cannot use more than
32 bits per pixel for performance reasons.

Chrome OS uses a peculiar EOTF for the blending space: the SDR range uses
so called gamma 2.2 EOTF and the HDR range above it uses a linear function.
This allows them to blend SDR and HDR content without conversions. Therefore it
does blending in essentially non-linear color space, with premultiplied alpha.

New use cases?

- Have two monitors in a mirrored setup, but use different (perceptual) color
  profiles for them, so that on one monitor you see the "real" colors and the
  other monitor shows you image color details you don't normally see due to the
  monitor having a small gamut.

HDR gamut metadata: pixel encoding uses one (standard) color space, but the
actual content gamut used is significantly smaller. A compositor needs to know
the pixel encoding to decode pixels, and it needs to know the gamut for better
`gamut mapping`_.

.. _`gamut mapping`: http://argyllcms.com/doc/iccgamutmapping.html
